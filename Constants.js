import {StyleSheet, Platform} from 'react-native'

const ACTIVE_STATUS = {
  IN_PROGRESS: 1,
  DONE: 2
}

const errorMessageTemplates = {
  incompleteItems : 'You have no Tasks',
  completeItems : `You haven't completed any Tasks`
}

const styles = StyleSheet.create({
  todoInput: {
    height: 50,
    borderWidth: 1,
    borderColor: "black",
    padding: 10,
    margin: 10
  },
  todoTile: {
    borderWidth: 1,
    borderColor: "gray",
    padding: 10,
    margin: 10
  },
  doneTodoTile: {
    borderWidth: 1,
    borderColor: "lightgray",
    padding: 10,
    margin: 10
  },
  todoActionButton: {
    width: '15%',
    left: '85%',
    paddingTop: 3,
    paddingBottom: 3,
  },
  todoActionButtonText: {
    textAlign: Platform.select({
      android: 'right',
      ios: 'center'
    })
  },
  todoText: {
    fontWeight: 'bold'
  },
  doneTodoText: {
    textDecorationLine: 'line-through', 
    textDecorationStyle: 'solid'
  },
  headerText: {
    fontWeight: 'bold',
    paddingLeft: 20,
    paddingTop: 5
  },
  errorMessageText: {
    fontStyle: 'italic',
    paddingLeft: 20,
    color: 'orange'
  },
  headerTextDanger: {
    fontWeight: 'bold',
    paddingLeft: 20,
    paddingTop: 5,
    color: 'firebrick'
  },
})

export {
  ACTIVE_STATUS,
  errorMessageTemplates,
  styles
}