import {Types} from './Actions'
import {ACTIVE_STATUS} from './Constants'

const INITIAL_STATE = {
  todoList: []
}

const todoItem = (item) => ({
  status: ACTIVE_STATUS.IN_PROGRESS,
  description: item
})

const addItem = (action, state) => ({
  ...state,
  todoList: [todoItem(action.item), ...state.todoList]
})

const updateItem = (item, status) => ({
  ...item,
  status
})

const updateItemStatus = (action, state) => ({
  ...state,
  todoList: state.todoList.map((item, i) => i === action.itemIndex ? updateItem(state.todoList[action.itemIndex], action.newItemStatus || ACTIVE_STATUS.IN_PROGRESS) : item)
})

const updateTodoList = (action, state) => ({
  ...state,
  todoList: action.todoList
})

const emptyList = (action, state) => ({
  ...state,
  todoList: []
})

const reducerMap = {
  [Types.ADD_ITEM]: addItem,
  [Types.UPDATE_ITEM_STATUS]: updateItemStatus,
  [Types.UPDATE_TODOLIST]: updateTodoList,
  [Types.EMPTY_LIST]: emptyList
}

const reducer = (state = INITIAL_STATE, action) => Boolean(action) && reducerMap[action.type] ? reducerMap[action.type](action, state) : state

export default reducer
