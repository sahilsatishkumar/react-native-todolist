const ADD_ITEM = 'ADD_ITEM'
const UPDATE_ITEM_STATUS = 'UPDATE_ITEM_STATUS'
const UPDATE_TODOLIST = 'UPDATE_TODOLIST'
const EMPTY_LIST = 'EMPTY_LIST'

const Types = {
  ADD_ITEM,
  UPDATE_ITEM_STATUS,
  UPDATE_TODOLIST,
  EMPTY_LIST
}

const addItem = (item) => ({
  type: ADD_ITEM,
  item
})

const updateItemStatus = (itemIndex, newItemStatus) => ({
  type: UPDATE_ITEM_STATUS,
  itemIndex,
  newItemStatus
})

const updateTodoList = (todoList) => ({
  type: UPDATE_TODOLIST,
  todoList
})

const emptyList = () => ({
  type: EMPTY_LIST
})

const Actions = {
  addItem,
  updateItemStatus,
  updateTodoList,
  emptyList
}

export {
  Types,
  Actions
}