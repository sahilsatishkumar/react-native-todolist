import React, { Component } from 'react';
import {Text, View, TextInput, Platform, AppState, AsyncStorage, TouchableOpacity, TouchableNativeFeedback, Alert, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {ACTIVE_STATUS, styles, errorMessageTemplates} from './Constants'

import {Actions} from './Actions'

const Button = Platform.select({
  ios: TouchableOpacity,
  android: TouchableNativeFeedback,
})

class TodoList extends Component {
  state = {
    todoItem : ''
  }

  addNewItemToTodoList = (todoItem = this.state.todoItem) => Boolean(todoItem) && Boolean(todoItem.trim()) ? this.setState({
    todoItem: ''
  }, () => this.props.addItem(todoItem)) : null

  handleItemComplete = (index) => this.props.updateItemStatus(index, ACTIVE_STATUS.DONE)

  handleItemInProgress = (index) => this.props.updateItemStatus(index, ACTIVE_STATUS.IN_PROGRESS) 

  getIncompleteList = (todoList = this.props.todoList) => todoList.map(item => (
    <View key={item.index} style={styles.todoTile}>
      <Text style={styles.todoText}>{item.description}</Text>
      <Button style={styles.todoActionButton} onPress={this.handleItemComplete.bind(this, item.index)}>
        <Text style={styles.todoActionButtonText}>Done?</Text>
      </Button>
    </View>
  ))

  getCompleteList = (todoList = this.props.todoList) => todoList.map(item => (
    <View key={item.index} style={styles.doneTodoTile}>
      <Text style={styles.doneTodoText}>{item.description}</Text>
      <Button style={styles.todoActionButton} onPress={this.handleItemInProgress.bind(this, item.index)}>
        <Text style={styles.todoActionButtonText}>☑️</Text>
      </Button>
    </View>
  ))

  getEmptyListError = (listType) => <Text style={styles.errorMessageText}>{errorMessageTemplates[listType]}</Text>

  emptyList = () => {
    Alert.alert(
      'Todo List',
      'Empty List ?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.props.emptyList()},
      ]
    )
  }

  getEmptyListMessage = (list1, list2) => (list1 === 0 && list2 > 4) ? (
    <Button onPress={() => this.emptyList()}>
      <Text style={styles.headerTextDanger}>Delete all items 🗑️</Text>
    </Button>
  ) : null

  render() {
    const {incompleteItems, completeItems} = this.props.todoList.reduce((itemMap, currentItem, index) => currentItem.status === ACTIVE_STATUS.IN_PROGRESS ? ({
      ...itemMap,
      incompleteItems: [...itemMap.incompleteItems, {...currentItem, index}]
    }) : ({
      ...itemMap,
      completeItems: [...itemMap.completeItems, {...currentItem, index}]
    }), {incompleteItems: [], completeItems: []})
    
    return (
      <ScrollView>
        <Text>{'\n'}</Text>
        <TextInput
          value={this.state.todoItem}
          style={styles.todoInput}
          placeholder={'Enter Item here....'}
          autoCorrect={false}
          underlineColorAndroid='transparent'
          onChangeText={todoItem => this.setState({todoItem})}
          onSubmitEditing={() => this.addNewItemToTodoList(this.state.todoItem)}
        />
        <Text style={styles.headerText}>Tasks to do</Text>
        {incompleteItems.length ? this.getIncompleteList(incompleteItems) : this.getEmptyListError('incompleteItems')}
        <Text style={styles.headerText}>Done tasks</Text>
        {completeItems.length ? this.getCompleteList(completeItems) : this.getEmptyListError('completeItems')}
        {this.getEmptyListMessage(incompleteItems.length, completeItems.length)}
        <Text>{'\n'}</Text>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  todoList: state.todoList
})

const mapDispatchToProps = (dispatch) => ({
  addItem: (item) => dispatch(Actions.addItem(item)),
  updateItemStatus: (itemIndex, itemStatus) => dispatch(Actions.updateItemStatus(itemIndex, itemStatus)),
  emptyList: () => dispatch(Actions.emptyList())
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)