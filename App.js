import React, { Component } from 'react';
import Store from './Store'
import TodoList from './TodoList'
import {Provider} from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

const {store, persistor} = Store()

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <TodoList/>
        </PersistGate>
      </Provider>
    );
  }
}
